from django.urls import path

from students.views import (StudentCreateView, StudentListView, StudentUpdateView, 
                            delete_student, generate_students)

app_name = 'students'

urlpatterns = [
    path('generate', generate_students, name='generator'),
    path('', StudentListView.as_view(), name='list'),
    path('create', StudentCreateView.as_view(), name='create'),
    path('edit/<uuid:uuid>', StudentUpdateView.as_view(), name='edit'),
    path('delete/<uuid:uuid>', delete_student, name='delete'),
    
    # path('delete/<uuid:uuid>', StudentDelete.as_view(), name='delete'),
    # path('', get_students, name='list'),
    # path('create', create_student, name='create'),
    # path('edit/<uuid:uuid>', edit_student, name='edit'),

]
