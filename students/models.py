import random
import uuid

from core.models import Person

from django.db import models

from faker import Faker

from groups.models import Group


class Student(Person):
    # Все поля ниже унаследованы от класса Person
    # id = models.AutoField(primary_key=True)
    # uuid = models.UUIDField(max_length=64, null=True, default=uuid.uuid4, unique=True)
    # first_name = models.CharField(max_length=64, null=True)
    # last_name = models.CharField(max_length=64, null=True)
    # email = models.CharField(max_length=64, null=True)
    # phone = models.CharField(max_length=14, null=True)
    rating = models.SmallIntegerField(null=True, default=0)
    group = models.ForeignKey(
        to=Group,
        null=True,
        on_delete=models.SET_NULL,
        related_name='students'
    )

    def __str__(self):
        return f'{self.id}, {self.first_name}, {self.last_name}, {self.email}, {self.rating}'

    @staticmethod
    def generate_students(count):
        fake = Faker()

        group_list = list(Group.object.all())

        for i in range(count):
            persons = Student(
                first_name=fake.first_name(),
                last_name=fake.last_name(),
                email=fake.email(),
                rating=random.randint(0, 9),
                phone=fake.phone_number(),
                uuid=uuid.uuid4(),
                group=random.choice(group_list)
            )
            persons.save()
        return ('Created ', count, ' students')


