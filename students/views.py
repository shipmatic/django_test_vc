# from core.utils import format_list
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator
from django.db.models import Q
from django.http import (HttpResponse, HttpResponseBadRequest,
                         HttpResponseRedirect)
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.urls.base import reverse_lazy
from django.views.generic import CreateView, DeleteView, ListView, UpdateView

from students.forms import StudentCreateForm, StudentEditForm
from students.models import Student  # для работы с базой данных
from students.utils import gen_password, parse_length

# from students.templates import 'students-list.html'


# Create your views here.


def hello(request):  # Вставляем аргумент request
    return HttpResponse('Hello from Django')


def get_random(request):
    try:
        length = parse_length(request, 10)
    except Exception as ex:
        return HttpResponse(str(ex), status_code=400)

    result = gen_password(length)

    return HttpResponse(result)


def generate_students(request, default=5):
    count = request.GET.get('count', str(default))

    if not count.isnumeric:
        raise HttpResponseBadRequest("VALUE ERROR: int")
    if int(count) > 100:
        raise HttpResponseBadRequest("RANGE ERROR: [3..100]")
    count = int(count)
    # Student.generate_students(count)

    return HttpResponse(Student.generate_students(count))


def get_students(request):

    students = Student.objects.select_related('group').all()

    paginator = Paginator(students, 5)
    page_number = request.GET.get('page', 1)
    page = paginator.get_page(page_number)

    first_name = request.GET.get('first_name')
    last_name = request.GET.get('last_name')
    email = request.GET.get('email')
    phone = request.GET.get('phone')
    # rating = request.GET.get('rating')

    if first_name:
        or_names = first_name.split('|')
        or_cond = Q()   # from django.db.models import Q
        for or_name in or_names:
            or_cond = or_cond | Q(first_name=or_name)
        students = students.filter(or_cond)

    if last_name:
        students = students.filter(last_name=last_name)

    if email:
        students = students.filter(email=email)

    if phone:
        students = students.filter(email=phone)     

    return render(
        request=request,
        template_name='students-list.html',
        # context={'students': format_list(students)}

        # context={'students': students,   
        #          # 'title': 'students list'
        #          }

        context={'students': page}


    )

    # return HttpResponse(format_list(students))


# New funct with dict
""" def get_students(request):
    students = Student.objects.all()

    params = [
        'first_name',
        'last_name',
        'birthdate',
        'rating'
    ]

    for param in params:
        value = request.GET.get(param)
        if value:
            students = students.filter(**{param: value})

    # ввод через HTML форму на сайте

    # уходим в файл students-list.html

    return render(
        request=request,
        template_name='students-list.html',
        context={
            'students': format_list(students)
        }
    )

    return HttpResponse(format_list(students)) """


def create_student(request):
    if request.method == 'GET':

        form = StudentCreateForm(request.POST)

    elif request.method == 'POST':

        form = StudentCreateForm(request.POST)

        if form.is_valid():
            form.save()
            # return HttpResponseRedirect('/students')
            return HttpResponseRedirect(reverse('students:list'))

    return render(
        request=request,
        template_name='students-create.html',
        context={
            'form': form,
            # 'title': 'Create Students'
        }
    )


def edit_student(request, uuid):
    try:
        student = Student.objects.get(uuid=uuid)
    except Student.DoesNotExist as ex:
        return HttpResponse('Student does not exist', status=404)

    if request.method == 'GET':

        form = StudentEditForm(instance=student)

    elif request.method == 'POST':

        form = StudentCreateForm(
            data=request.POST,
            instance=student
        )

        if form.is_valid():
            form.save()

            # from django.urls import reverse
            # return HttpResponseRedirect('/students')
            return HttpResponseRedirect(reverse('students:list'))

    return render(
        request=request,
        template_name='students-edit.html',
        context={
            'form': form,
            'student': student,
            # 'title': 'Edit Student'
        }
    )

    # return HttpResponse('EDIT')


def delete_student(request, uuid):

    student = get_object_or_404(Student, uuid=uuid)

    student.delete()

    return HttpResponseRedirect(reverse('students:list'))


class StudentUpdateView(LoginRequiredMixin, UpdateView):
    login_url = reverse_lazy('accounts:login')
    model = Student
    form_class = StudentEditForm
    template_name = 'students-edit.html'
    success_url = reverse_lazy('students:list')
    context_object_name = 'student'
    pk_url_kwarg = 'uuid'

    def get_object(self):
        uuid = self.kwargs.get('uuid')
        return self.get_queryset().get(uuid=uuid)


class StudentListView(LoginRequiredMixin, ListView):
    login_url = reverse_lazy('accounts:login')
    model = Student
    template_name = 'students-list.html'
    context_object_name = 'students'
    paginate_by = 5
    
    def get_queryset(self):
        # qs = self.model.objects.all()
        qs = super().get_queryset()
        qs = qs.select_related('group')
        first_name = self.request.GET.get('first_name')
        last_name = self.request.GET.get('last_name')
        email = self.request.GET.get('email')
        phone = self.request.GET.get('phone')
        rating = self.request.GET.get('rating')
        if last_name:
            qs = Student.objects.filter(last_name=last_name)
        if first_name:
            qs = Student.objects.filter(first_name=first_name)
        if email:
            qs = Student.objects.filter(email=email)
        if phone:
            qs = Student.objects.filter(phone=phone)
        if rating:
            qs = Student.objects.filter(rating=rating)

        return qs


class StudentCreateView(LoginRequiredMixin, CreateView):
    login_url = reverse_lazy('accounts:login')
    model = Student
    form_class = StudentCreateForm
    template_name = 'students-list.html'
    context_object_name = 'students'


class StudentDelete(LoginRequiredMixin, DeleteView):
    login_url = reverse_lazy('accounts:login')
    model = Student
    success_url = reverse_lazy('students:list')
    pk_url_kwarg = 'uuid'


