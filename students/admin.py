# from django.contrib import admin

# Register your models here.

from django.contrib import admin

from students.models import Student


class StudentAdmin(admin.ModelAdmin):
    exclude = ['uuid']


admin.site.register(Student, StudentAdmin)
