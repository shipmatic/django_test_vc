from django import forms
from django.core.exceptions import ValidationError

from students.models import Student


class StudentBaseForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = ['first_name', 'last_name', 'rating', 'email', 'phone', 'group']  # removed 'uuid'
  
    def clean_email(self):  # Функция для проверки валидности адреса
        email = self.cleaned_data['email']

        blacklist_domains = [
            'mail.ru'
            'yandex.ru'
        ]

        domain = email.split('@')[1]

        if domain in blacklist_domains:
            raise ValidationError('Restricted domain')

        return email
    
    def clean_phone(self):
        phone = self.cleaned_data.get('phone', None)
        try:
            int(phone)
        except (ValueError, TypeError):
            raise ValidationError('Please enter a valid phone number')
        
        return phone


class StudentCreateForm(StudentBaseForm):
    class Meta(StudentBaseForm.Meta):
        model = Student
        fields = ['first_name', 'last_name', 'rating', 'email', 'group']  
            

class StudentEditForm(StudentBaseForm):
    class Meta(StudentBaseForm.Meta):
        model = Student
        fields = ['first_name', 'last_name', 'rating', 'email', 'group']          
              
        
