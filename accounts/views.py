# import datetime
from accounts.forms import (AccountCreateForm, AccountProfileUpdateForm,
                            AccountUpdateForm)
from accounts.models import UserActions

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse_lazy
from django.urls.base import reverse
from django.views.generic import CreateView  # UpdateView
from django.views.generic.edit import ProcessFormView


class AccountCreateView(CreateView):
    # login_url = reverse_lazy('accounts:login')
    model = User
    template_name = 'registration.html'
    form_class = AccountCreateForm
    success_url = reverse_lazy('accounts:login')  # это редирект после завершения регистрации

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.success(self.request, 'User successfully created')
        return result


class AccountLoginView(LoginView):
    model = UserActions
    template_name = 'login.html'
    success_url = reverse_lazy('core:index')

    # функция ниже переправляет на страницу откужа пришел пользователь
    # Если сработал LoginRequiredMixin

    def get_redirect_url(self):
        if self.request.GET.get('next'):  # это параметр Get запроса проверяет, есть ли 
            return self.request.GET.get('next')  # next в адресной строке
        return reverse('core:index')

    def form_valid(self, form):

        result = super().form_valid(form)
        record = UserActions.objects.create(
            user=self.request.user,
            action=UserActions.USER_ACTION.LOGIN,
            info='Any information'
        )
        record.save()
        messages.info(self.request, f'User {self.request.user} logged in!')

        # Код ниже создает профиль пользователя он заккоментрован, работают сигналы

        # try:
        #     profile = self.request.user.profile
        # except BaseException:
        #     profile = Profile.objects.create(user=self.request.user)
        #     profile.save()

        return result


class AccountLogoutView(LogoutView):
    template_name = 'logout.html'
    # success_message = 'User Logged Out'

    def get(self, request, *args, **kwargs):
        result = super().get(request, *args, **kwargs)
        messages.info(self.request, f'User {self.request.user} has been logged out!')
        return result


# class AccountUpdateView(LoginRequiredMixin, UpdateView):
#     login_url = reverse_lazy('accounts:login')
#     model = User
#     template_name = 'profile.html'
#     form_class = AccountUpdateForm
#     success_url = reverse_lazy('core:index')  

#     def get_object(self, queryset=None):
#         return self.request.user

class AccountPasswordView(LoginRequiredMixin, PasswordChangeView):
    login_url = reverse_lazy('accounts:login')
    template_name = 'password.html'
    success_url = reverse_lazy('core:index')  


class AccountUpdateView(LoginRequiredMixin, ProcessFormView):

    def get(self, request, *args, **kwargs):
        user = self.request.user
        profile = self.request.user.profile

        user_form = AccountUpdateForm(instance=user)
        profile_form = AccountProfileUpdateForm(instance=profile)

        return render(
            request=request,
            template_name='profile.html',
            context={'user_form': user_form,
                     'profile_form': profile_form, })

    def post(self, request, *args, **kwargs):
        user = self.request.user
        profile = self.request.user.profile  # noqa
        
        user_form = AccountUpdateForm(data=request.POST,
                                      instance=user)
        profile_form = AccountProfileUpdateForm(data=request.POST,
                                                files=request.FILES, 
                                                instance=profile)

        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            old_pic = profile.image.name  # HW
            new_pic = profile_form.cleaned_data['image'].name
            if old_pic != new_pic:
                record = UserActions.objects.create(
                    user=self.request.user,
                    action=4,
                    info=f'{old_pic} -> {new_pic}'
                )
                record.save()
            # End HW    
            messages.info(self.request, 'Account Updated')
            return HttpResponseRedirect(reverse('accounts:profile'))

        return render(
            request=request,
            template_name='profile.html',
            context={'user_form': user_form,
                     'profile_form': profile_form, })

