from accounts.models import Profile

from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.forms.models import ModelForm


class AccountCreateForm(UserCreationForm):
    # pass
    class Meta(UserCreationForm.Meta):
        fields = ['username', 'first_name', 'last_name', 'email']


class AccountUpdateForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        fields = ['username', 'first_name', 'last_name', 'email']


class AccountProfileUpdateForm(ModelForm):
    class Meta:
        model = Profile
        # fields = '__all__'
        fields = ['image', 'interests']



