from PIL import Image

from django.contrib.auth.models import User
from django.db import models

from lms import settings


class UserActions(models.Model):
    class USER_ACTION(models.IntegerChoices):
        LOGIN = 0, "Login"
        LOGOUT = 1, "Logout"
        CHANGE_PASSWORD = 2, "Change Password"
        CHANGE_PROFILE = 3, "Change Profile"
        CHANGE_PROFILE_IMAGE = 4, "Change Profile Image"

    user = models.ForeignKey(to=User, on_delete=models.CASCADE)
    write_date = models.DateTimeField(auto_now_add=True)
    action = models.PositiveSmallIntegerField(choices=USER_ACTION.choices)
    info = models.CharField(max_length=128, null=True)


class Profile(models.Model):
    user = models.OneToOneField(
        to=User,  # settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name="profile"
    )
    image = models.ImageField(null=True, default='default.jpg', upload_to='pics/') 
    interests = models.CharField(max_length=128, null=True)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        # img = Image.open(self.image.name)
        # img = Image.open(settings.MEDIA_ROOT + 'pics/' + self.image.name)
        img = Image.open(settings.MEDIA_ROOT + self.image.name)
        # img = self.cleaned_data['image']
        if img.width == img.height:
            resizedImage = img.resize((round(img.size[0] * .3), round(img.size[1] * .3)))
        else:
            resizedImage = img.resize((round(img.size[0] * .3), round(img.size[1] * .2)))
 
        resizedImage.save(self.image.name)
        