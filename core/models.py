import datetime
import uuid

from django.contrib.auth.models import User
from django.db import models


class BaseModel(models.Model):
    class Meta:
        abstract = True
    create_date = models.DateField(auto_now_add=True, null=True)
    write_date = models.DateField(null=True)
    
    def save(self, *args, **kwargs):
        self.write_date = datetime.datetime.now()
        super().save(*args, **kwargs)
        

class Person(BaseModel):
    class Meta:
        abstract = True
    uuid = models.UUIDField(max_length=64, null=True, default=uuid.uuid4, unique=True)
    first_name = models.CharField(max_length=64, null=True)
    last_name = models.CharField(max_length=64, null=True)
    email = models.CharField(max_length=64, null=True)
    phone = models.CharField(max_length=14, null=True)
    birthdate = models.DateField(null=True, default=datetime.date.today)

    def full_name(self):
        return f'{self.first_name}, {self.last_name}'

    def age(self):
        return datetime.datetime.now().date().year - self.birthdate.year

    def __str__(self):
        return f'{self.id}, {self.full_name()}, {self.age()}'


class Logger(models.Model):
    user = models.ForeignKey(to=User, on_delete=models.CASCADE)
    path = models.CharField(max_length=128)
    create_date = models.DateTimeField(auto_now_add=True)
    execution_time = models.FloatField()
    query_params = models.CharField(max_length=64, null=True)
    info = models.CharField(max_length=128, null=True)

