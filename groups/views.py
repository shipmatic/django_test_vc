# from core.utils import format_list

from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator
from django.db.models import Q
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, DeleteView, ListView, UpdateView

from groups.forms import GroupCreateForm, GroupEditForm
from groups.models import Group

# Create your views here.


def groups_list(request):
    # groups = Group.objects.all()
    groups = Group.objects.select_related('teacher').all()

    paginator = Paginator(groups, 5)
    page_number = request.GET.get('page', 1)
    page = paginator.get_page(page_number)

    name = request.GET.get('name')
    program = request.GET.get('program')

    if name:
        or_names = name.split('|')
        or_cond = Q()   # from django.db.models import Q
        for or_name in or_names:
            or_cond = or_cond | Q(name=or_name)
        groups = groups.filter(or_cond)

    if program:
        groups = groups.filter(program=program)

    return render(
        request=request,
        template_name='groups-list.html',
        # context={'groups': format_list(groups)}
        # context={'groups': groups}
        context={'groups': page}

    )


# def groups_list(request):
#     groups = Group.objects.all()

#     params = [
#         'name',
#         'program'

#     ]

#     for param in params:
#         value = request.GET.get(param)
#         if value:
#             groups = groups.filter(**{param: value})
#     return HttpResponse(format_list(groups))


def create_group(request):

    if request.method == "GET":

        form = GroupCreateForm()

        return render(
            request=request,
            template_name='groups-create.html',
            context={'form': form}
        )

    elif request.method == 'POST':

        form = GroupCreateForm(request.POST)

        if form.is_valid():
            form.save()
            # return HttpResponseRedirect('/groups')
            return HttpResponseRedirect(reverse('groups:list'))

    return render(
        request=request,
        template_name='groups-create.html',
        context={'form': form}
    )

    # return HttpResponseRedirect('/groups/')


def edit_groups(request, id):
    try:
        group = Group.objects.get(id=id)
    except Group.DoesNotExist as ex:
        return HttpResponse('Group does not exist', status=404)

    if request.method == 'GET':

        form = GroupEditForm(instance=group)

    elif request.method == 'POST':

        form = GroupCreateForm(
            data=request.POST,
            instance=group
        )

        if form.is_valid():
            form.save()
            # return HttpResponseRedirect('/groups')
            return HttpResponseRedirect(reverse('groups:list'))

    return render(
        request=request,
        template_name='groups-edit.html',  # Это передается в шаблон html
        context={
            'form': form,
            'group': group,
            'students': group.students.all()
        }
    )


def delete_group(request, id):

    group = get_object_or_404(Group, id=id)

    group.delete()

    return HttpResponseRedirect(reverse('groups:list'))  


def generate_groups(request, default=5):
    count = request.GET.get('count', str(default))

    if not count.isnumeric:
        raise HttpResponseBadRequest("VALUE ERROR: int")
    if int(count) > 100:
        raise HttpResponseBadRequest("RANGE ERROR: [3..100]")
    count = int(count)
    # Student.generate_students(count)

    return HttpResponse(Group.generate_groups(count))


class GroupUpdateView(LoginRequiredMixin, UpdateView):
    login_url = reverse_lazy('accounts:login')
    model = Group
    form_class = GroupEditForm
    template_name = 'groups-edit.html'
    success_url = reverse_lazy('groups:list')
    context_object_name = 'group'
    pk_url_kwarg = 'id'
    
    def get_object(self):
        id = self.kwargs.get('id')
        return self.get_queryset().get(id=id)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['students'] = self.get_object().students.all()
        return context


class GroupListView(LoginRequiredMixin, ListView):
    login_url = reverse_lazy('accounts:login')
    model = Group
    template_name = 'groups-list.html'
    context_object_name = 'groups'
    paginate_by = 5

    def get_queryset(self):
        qs = self.model.objects.all()
        # qs = super().get_queryset()
        name = self.request.GET.get('name')
        program = self.request.GET.get('program')
        if name:
            qs = Group.objects.filter(name=name)
        if program:
            qs = Group.objects.filter(program=program)
        
        return qs  


class GroupCreateView(LoginRequiredMixin, CreateView):
    login_url = reverse_lazy('accounts:login')
    model = Group
    form_class = GroupCreateForm
    template_name = 'groups-create.html'
    context_object_name = 'groups'
    success_url = reverse_lazy('groups:list')
    

class GroupDeleteView(DeleteView):
    model = Group
    pk_url_kwarg = 'id'
    success_url = reverse_lazy('groups:list')
    