from django.urls import path

from groups.views import (GroupCreateView, GroupDeleteView, GroupListView, 
                          GroupUpdateView, generate_groups, 
                          )

app_name = 'groups'

urlpatterns = [
    # path('', groups_list, name='list'),
    path('', GroupListView.as_view(), name='list'),
    # path('create', create_group, name='create'),
    path('create', GroupCreateView.as_view(), name='create'),
    # path('edit/<int:id>', edit_groups, name='edit'),
    path('edit/<int:id>', GroupUpdateView.as_view(), name='edit'),
    # path('delete/<int:id>', delete_group, name='delete'),
    path('delete/<int:id>', GroupDeleteView.as_view(), name='delete'),
    path('generate', generate_groups, name='generator'),

]
