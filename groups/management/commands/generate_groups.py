from django.core.management.base import BaseCommand

from faker import Faker

from groups.models import Group


class Command(BaseCommand):
    help = 'create specified numbers of groups'

    def add_arguments(self, parser):

        # Указываем сколько и каких аргументов принимает команда.
        # В данном случае, это один аргумент типа int.

        parser.add_argument('groups_count', nargs=1, type=int)

    def handle(self, *args, **options):
        fake = Faker()
        # Получаем аргумент, создаём необходимое количество groups
        # и выводим сообщение об успешном завершении генерирования

        groups_count = options['groups_count'][0]

        Group.generate_groups(groups_count)

        """ for i in range(teachers_count):
            persons = Teacher(
                first_name=fake.first_name(),
                last_name=fake.last_name(),
                prof=fake.job())
            persons.save() """

        self.stdout.write(
            'Successfully created {0} groups!'.format(groups_count))
