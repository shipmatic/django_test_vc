# from django.contrib import admin

# Register your models here.

from django.contrib import admin

from groups.models import Classroom, Group

from students.models import Student


class StudentTable(admin.TabularInline):
    model = Student()
    fields = ['first_name', 'last_name', 'email']
    show_change_link = True


class GroupAdmin(admin.ModelAdmin):
    list_display = ['name', 'course', 'head']
    fields = ['name', 'course', 'head', 'classroom']
    inlines = [StudentTable]
    list_select_related = ['head']


admin.site.register(Classroom)
admin.site.register(Group)
