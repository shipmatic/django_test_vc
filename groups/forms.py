from django import forms

from groups.models import Group


class GroupBaseForm(forms.ModelForm):
    class Meta:
        model = Group
        # fields = ['name', 'course', 'teacher', 'supervisor']
        fields = '__all__'  # включает все поля в форму
        exclude = ['start_date', 'uuid']
        

class GroupCreateForm(GroupBaseForm):
    class Meta(GroupBaseForm.Meta):
        model = Group
        fields = ['name', 'course', 'teacher', 'supervisor']
        # fields = '__all__'  # включает все поля в форму
        # exclude = ['start_date', 'uuid', 'head']


class GroupEditForm(GroupBaseForm):
    class Meta(GroupBaseForm.Meta):
        model = Group
        # fields = ['name', 'course', 'teacher', 'supervisor']
        fields = '__all__'  # включает все поля в форму
        exclude = ['start_date', 'uuid']
                     
    