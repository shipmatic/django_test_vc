import datetime
import random

from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.forms import ValidationError

from faker import Faker

# from groups.views import GroupUpdateView

from teachers.models import Teacher


# Create your models here.


class Classroom(models.Model):
    name = models.CharField(max_length=32)
    floor = models.PositiveSmallIntegerField(
        validators=[MinValueValidator(2), MaxValueValidator(5)]
    )

    def __str__(self):
        return f'{self.name}, {self.floor}'


class Group(models.Model):
    name = models.CharField(max_length=64, null=False)
    course = models.CharField(max_length=64, null=False)
    start_date = models.DateField(null=True, default=datetime.date.today)
    teacher = models.ForeignKey(
        to=Teacher,
        null=True,
        on_delete=models.SET_NULL,
        related_name='groups',
        # default=random.choice(list(Teacher.objects.all()))
    )
    supervisor = models.OneToOneField(
        to=Teacher,
        null=True,
        on_delete=models.SET_NULL,
        related_name='supervisor'
    )
    head = models.OneToOneField(
        to='students.Student',
        null=True,
        on_delete=models.SET_NULL,
        related_name='head_of_group'
    )
    classrooms = models.ManyToManyField(
        to=Classroom,
        related_name='groups'
    )

    def __str__(self):
        return f'{self.id}, {self.name}, {self.course}'

    @staticmethod
    def generate_groups(count):
        fake = Faker()
        teacher_list = list(Teacher.objects.all())
        for i in range(count):
            groups = Group(
                name=fake.bs(),
                course=fake.job(),
                teacher=random.choice(teacher_list)
            )
            groups.save()
        return ('Created ', count, ' groups')

    def clean_head(self):
        cleaned_data = super().clean()
        data = self.cleaned_data.get('head')
        data = self.head
        if data not in self.students.all():
            
            raise ValidationError('Head must be group member')

        else:
            return data

    def save(self, *args, **kwargs):

        # self.clean_head()
        super().save(*args, **kwargs)
