# Generated by Django 3.1 on 2020-09-07 14:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('groups', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('group_name', models.CharField(max_length=64)),
                ('group_program', models.CharField(max_length=64)),
            ],
        ),
        migrations.DeleteModel(
            name='Groups',
        ),
    ]
