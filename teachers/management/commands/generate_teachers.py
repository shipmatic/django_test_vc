from django.core.management.base import BaseCommand

from faker import Faker

from teachers.models import Teacher


class Command(BaseCommand):
    help = 'create specified numbers of teachers'

    def add_arguments(self, parser):

        # Указываем сколько и каких аргументов принимает команда.
        # В данном случае, это один аргумент типа int.

        parser.add_argument('teachers_count', nargs=1, type=int)

    def handle(self, *args, **options):
        fake = Faker()
        # Получаем аргумент, создаём необходимое количество teachers
        # и выводим сообщение об успешном завершении генерирования

        teachers_count = options['teachers_count'][0]

        Teacher.generate_teachers(teachers_count)

        """ for i in range(teachers_count):
            persons = Teacher(
                first_name=fake.first_name(),
                last_name=fake.last_name(),
                prof=fake.job())
            persons.save() """

        self.stdout.write(
            'Successfully created {0} teachers!'.format(teachers_count))
