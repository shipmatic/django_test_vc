from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
from django.http import HttpResponseBadRequest
from django.shortcuts import get_object_or_404, render
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView

from teachers.forms import TeacherCreateForm, TeacherEditForm
from teachers.models import Teacher


# Create your views here.


def teachers_list(request):
    teachers = Teacher.objects.all()

    paginator = Paginator(teachers, 5)
    page_number = request.GET.get('page', 1)
    page = paginator.get_page(page_number)

    first_name = request.GET.get('first_name')
    last_name = request.GET.get('last_name')
    prof = request.GET.get('prof')

    if first_name:
        or_names = first_name.split('|')
        or_cond = Q()   # from django.db.models import Q
        for or_name in or_names:
            or_cond = or_cond | Q(first_name=or_name)
        teachers = teachers.filter(or_cond)

    if last_name:
        teachers = teachers.filter(last_name=last_name)

    if prof:
        teachers = teachers.filter(prof=prof)

    return render(
        request=request,
        template_name='teachers-list.html',
        # context={'teachers': format_list(teachers)}
        # context={'teachers': teachers}
        context={'teachers': page}

    )


# def teachers_list(request):
#     teachers = Teacher.objects.all()

#     params = [
#         'first_name',
#         'last_name',
#         'prof'

#     ]

#     for param in params:
#         value = request.GET.get(param)
#         if value:
#             teachers = teachers.filter(**{param: value})
#     return HttpResponse(format_list(teachers))


def generate_teachers(request, default=5):
    count = request.GET.get('count', str(default))

    if not count.isnumeric:
        raise HttpResponseBadRequest("VALUE ERROR: int")
    if int(count) > 100:
        raise HttpResponseBadRequest("RANGE ERROR: [3..100]")

    count = int(count)

    return HttpResponse(Teacher.generate_teachers(count))

# Вариант функции с использованием GET запросов и тегов форм в html шаблоне


def teacher_create_get(request):
    if not request.GET:
        return render(
            request=request,
            template_name='teachers-create.html',


        )
        HttpResponseRedirect('/teachers/')
    else:
        first_name = request.GET.get('first_name')
        last_name = request.GET.get('last_name')
        prof = request.GET.get('prof')

        tea = Teacher.objects.create(
            first_name=first_name,
            last_name=last_name,
            prof=prof
        )
        tea.save()

        # return HttpResponseRedirect('/teachers/')
        return HttpResponseRedirect(reverse(teachers_list))


# Вариант функции с использованием класса Django forms, и файла forms.py


def create_teacher(request):

    if request.method == 'GET':
        form = TeacherCreateForm()
        return render(
            request=request,
            template_name='teachers-create.html',
            context={'form': form},
        )
        HttpResponseRedirect('/teachers/')
    elif request.method == 'POST':
        form = TeacherCreateForm(request.POST)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('teachers:list'))

        # Блок ниже делает все тоже самое, что блок наверху .

        """ first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        teacher_prof = request.POST.get('teacher_prof')

        tea = Teacher.objects.create(
            first_name=first_name,
            last_name=last_name,
            prof=prof
        )
        tea.save()

        return HttpResponseRedirect('/teachers/')
     """


def edit_teachers(request, id):
    try:
        teacher = Teacher.objects.get(id=id)
    except Teacher.DoesNotExist as ex:
        return HttpResponse('Teacher does not exist', status=404)

    if request.method == 'GET':

        form = TeacherEditForm(instance=teacher)

    elif request.method == 'POST':

        form = TeacherCreateForm(
            data=request.POST,
            instance=teacher
        )

        if form.is_valid():
            form.save()
            # return HttpResponseRedirect('/teachers')
            return HttpResponseRedirect(reverse('teachers:list'))

    return render(
        request=request,
        template_name='teachers-edit.html',
        context={
            'form': form,
            'teacher': teacher,
            'groups': teacher.groups.all()
        }
    )

    return HttpResponse('EDIT')


def delete_teacher(request, id):

    teacher = get_object_or_404(Teacher, id=id)

    teacher.delete()

    return HttpResponseRedirect(reverse('teachers:list'))    


class TeacherUpdateView(LoginRequiredMixin, UpdateView):
    login_url = reverse_lazy('accounts:login')
    model = Teacher
    form_class = TeacherEditForm
    template_name = 'teachers-edit.html'
    success_url = reverse_lazy('teachers:list')
    context_object_name = 'teacher'
    pk_url_kwarg = 'id'

    def get_object(self):
        id = self.kwargs.get('id')
        return self.get_queryset().get(id=id)


class TeacherListView(LoginRequiredMixin, ListView):
    login_url = reverse_lazy('accounts:login')
    model = Teacher
    template_name = 'teachers-list.html'
    context_object_name = 'teachers'
    paginate_by = 5

    def get_queryset(self):
        qs = self.model.objects.all()
        
        first_name = self.request.GET.get('first_name')
        last_name = self.request.GET.get('last_name')
        prof = self.request.GET.get('prof')
        if last_name:
            qs = Teacher.objects.filter(last_name=last_name)
        if first_name:
            qs = Teacher.objects.filter(first_name=first_name)
        if prof:
            qs = Teacher.objects.filter(prof=prof)
        
        return qs


class TeacherCreateView(LoginRequiredMixin, CreateView):
    login_url = reverse_lazy('accounts:login')
    model = Teacher
    form_class = TeacherCreateForm
    template_name = 'teachers-list.html'
    context_object_name = 'teachers'
