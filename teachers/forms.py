from django import forms

from teachers.models import Teacher


class TeacherBaseForm(forms.ModelForm):
    class Meta:
        model = Teacher
        fields = ['first_name', 'last_name', 'prof']


class TeacherCreateForm(TeacherBaseForm):
    class Meta(TeacherBaseForm.Meta):
        model = Teacher
        fields = ['first_name', 'last_name', 'prof']


class TeacherEditForm(TeacherBaseForm):
    class Meta(TeacherBaseForm.Meta):
        model = Teacher
        fields = ['first_name', 'last_name', 'prof']
        