# from django.contrib import admin

# Register your models here.


from django.contrib import admin

# from groups.models import Group

from groups.models import Group

from teachers.models import Teacher


class GroupList(admin.TabularInline):
    model = Group
    fields = ['name', 'course']
    show_change_link = False
    fk_name = 'teacher'


class TeacherAdmin(admin.ModelAdmin):
    list_display = ['first_name', 'last_name', 'prof']
    fields = ['first_name', 'last_name', 'prof']
    inlines = [GroupList]


admin.site.register(Teacher, TeacherAdmin)
