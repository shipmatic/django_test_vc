import uuid

from core.models import Person

from django.db import models

from faker import Faker


class Teacher(Person):
    # first_name = models.CharField(max_length=64, null=False)
    # last_name = models.CharField(max_length=64, null=False)
    prof = models.CharField(max_length=64, null=False)
    
    def __str__(self):

        return f'{self.id}, {self.first_name}, {self.last_name}, {self.prof}'

    @staticmethod
    def generate_teachers(count):
        fake = Faker()

        for i in range(count):
            persons = Teacher(
                first_name=fake.first_name(),
                last_name=fake.last_name(),
                email=fake.email(),
                phone=fake.phone_number(),
                uuid=uuid.uuid4(),
                prof=fake.job())
            persons.save()
        return ('Created ', count, ' teachers')
        
    # def save(self, *args, **kwargs):
    #     self.write_date = datetime.date.today()
    #     super().save(*args, **kwargs)
        
