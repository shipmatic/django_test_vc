from django.urls import path

from teachers.views import (TeacherCreateView, TeacherListView, TeacherUpdateView, 
                            delete_teacher, generate_teachers,
                            )


app_name = 'teachers'

urlpatterns = [
    path('generate', generate_teachers, name='generator'),
    # path('create', create_teacher, name='create'),
    path('create', TeacherCreateView.as_view(), name='create'),
    # path('', teachers_list, name='list'),
    path('', TeacherListView.as_view(), name='list'),
    # path('edit/<int:id>', edit_teachers, name='edit'),
    path('edit/<int:id>', TeacherUpdateView.as_view(), name='edit'),
    path('delete/<int:id>', delete_teacher, name='delete'),
    
]
