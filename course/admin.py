from course.models import Course, Technology

from django.contrib import admin


# Register your models here.

class CourseTable(admin.ModelAdmin):
    list_display = ['name', 'technology']
    fields = ['name', 'technology']


admin.site.register(Course)
admin.site.register(Technology)
