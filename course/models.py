from django.db import models

# Create your models here.


class Technology(models.Model):
    name = models.CharField(max_length=32)

    def __str__(self):
        return f'{self.name}'


class Course(models.Model):
    name = models.CharField(max_length=32)
    technology = models.ManyToManyField(
        to=Technology,
        related_name='course'
    )

    def __str__(self):
        return f'{self.name}, {self.technology}'




